//
//  ContentView.swift
//  Calc
//
//  Created by Ananth Chepuri on 07/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var email = ""
    @State var password = ""
    @State var selection: Int? = nil

    var body: some View {
        NavigationView {
        VStack (alignment: .center, spacing: 15) {
            Image("PK-Red-850x850")
                .resizable()
                .frame(width: 350, height: 350, alignment: .center)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 2))
                .shadow(radius: 5)
                .padding(.bottom, 10)

            TextField("Email", text: self.$email)
                .frame(height: 20, alignment: .center)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 14)
                        .stroke(Color.gray, lineWidth: 1)
                )
            SecureField("Password", text: self.$password)
                .frame(height: 20, alignment: .center)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 14)
                        .stroke(Color.gray, lineWidth: 1)
                )
            
//            Button(action: {
//                // What to perform
//                self.clickMe()
//            }) {
//                // How the button looks like
//                Text("Sign In!")
//                .font(.headline)
//                .foregroundColor(.white)
//                .padding()
//                    .frame(width: 300, height: 50, alignment: .center)
//                .background(Color.green)
//                .cornerRadius(14.0)
//            }
            
            NavigationLink(destination: HomePage(), tag: 1, selection: $selection) {
                Button(action: {
                    // What to perform
                    self.clickMe()
                    self.selection = 1
                }) {
                    // How the button looks like
                    Text("Sign In!")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                        .frame(width: 300, height: 50, alignment: .center)
                    .background(Color.green)
                    .cornerRadius(14.0)
                }
            }
            
//            NavigationLink(destination: HomePage()) {
//               Text("Press on me")
//            }.buttonStyle(PlainButtonStyle())
        }
        .padding([.leading, .trailing], 20)
    }
}
    
    func clickMe() {
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

