//
//  DetailPage.swift
//  Login
//
//  Created by Ananth Chepuri on 09/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct DetailPage: View {
    @State var itemImage: String = ""
    
    var body: some View {
        VStack {
            Image(itemImage)
            Text(itemImage)
        }
    }
}

struct DetailPage_Previews: PreviewProvider {
    static var previews: some View {
        DetailPage()
    }
}
