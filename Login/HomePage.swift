//
//  HomePage.swift
//  Login
//
//  Created by Ananth Chepuri on 08/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct ImageModel: Identifiable {
    var id: Int
    let coffeeImage: String
}

struct TeaImageModel: Identifiable {
    var id: Int
    let teaImage: String
}

struct JuiceImageModel: Identifiable {
    var id: Int
    let juiceImage: String
}

struct HomePage: View {
    
    @State var isActive: Bool = false

    var coffeeList = [
        ImageModel(id: 1, coffeeImage: "Latte"),
        ImageModel(id: 2, coffeeImage: "White Espresso"),
        ImageModel(id: 3, coffeeImage: "Flat white"),
        ImageModel(id: 4, coffeeImage: "Mochaccino"),
        ImageModel(id: 5, coffeeImage: "Long Black")
    ]
    
    var teaList = [
        TeaImageModel(id: 1, teaImage: "Green Tea"),
        TeaImageModel(id: 2, teaImage: "Black Tea"),
        TeaImageModel(id: 3, teaImage: "Herbal Tea"),
        TeaImageModel(id: 4, teaImage: "White Tea"),
        TeaImageModel(id: 5, teaImage: "Fermented Tea"),
        TeaImageModel(id: 4, teaImage: "Yellow Tea")
    ]
    
    var juiceList = [
        JuiceImageModel(id: 1, juiceImage: "Grape juice"),
        JuiceImageModel(id: 2, juiceImage: "Banana juice"),
        JuiceImageModel(id: 3, juiceImage: "Apple juice"),
        JuiceImageModel(id: 4, juiceImage: "Pine Apple juice"),
        JuiceImageModel(id: 5, juiceImage: "Guava juice"),
        JuiceImageModel(id: 4, juiceImage: "Mosambi juice")
    ]
    
    var body: some View {
        VStack (alignment: .leading) {
            Text(" Choose a Coffee...")
            ScrollView(.horizontal) {
                HStack(spacing: 20) {
                    ForEach(coffeeList, id: \.coffeeImage) { coffee in
                        VStack {
                            NavigationLink(destination: DetailPage(itemImage: coffee.coffeeImage), isActive: self.$isActive) {
                                Text("")
                            }
                            Button(action: {
                                print("Scroll Button")
                                self.isActive = true
                            })
                              {
                                Image(coffee.coffeeImage)
                                    .resizable()
                                    .frame(width: 180, height: 180, alignment: .center)
                            }
                            .buttonStyle(PlainButtonStyle())
                            Text(coffee.coffeeImage)
                        }
                    }
                }
            }
            Divider()
            Text(" Choose a Tea...")
            ScrollView(.horizontal) {
                HStack(spacing: 20) {
                    ForEach(teaList, id: \.teaImage) { tea in
                        VStack {
                        Image(tea.teaImage)
                            .resizable()
                            .frame(width: 180, height: 180, alignment: .center)
                        Text(tea.teaImage)
                        }
                    }
                }
            }
            Divider()
            Text(" Choose a Juice...")
            ScrollView(.horizontal) {
                HStack(spacing: 20) {
                    ForEach(juiceList, id: \.juiceImage) { juice in
                        VStack {
                        Image(juice.juiceImage)
                            .resizable()
                            .frame(width: 180, height: 180, alignment: .center)
                        Text(juice.juiceImage)
                        }
                    }
                }
            }
        }
    }
}

struct HomePage_Previews: PreviewProvider {
    static var previews: some View {
        HomePage()
    }
}
